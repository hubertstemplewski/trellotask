import React, { Component } from 'react';

class List extends Component {
	render() {
		const ListElements = this.props.listsArray.listElements.map((items, i) => {
			const id = this.props.id+" col"
			return (
					<div
						id={i+" row"}
	          draggable="true"
						onDragOver={this.props.handleOnDragOver}
	          onDragStart={this.props.handleOnDragRowStart(id)}
	          onDrop={this.props.handleOnDropRow(id)}
	          style={{cursor: 'pointer'}}  
						className="list-card">
						<p>{items.content}</p>
					</div>
			);
		})
		return (
				<div 
					id={this.props.id+" col"}
          draggable="true"
					onDragOver={this.props.handleOnDragOver}
          onDragStart={this.props.handleOnDragColumnStart}
          onDrop={this.props.handleOnDropColumn}
          style={{cursor: 'pointer'}}
					className="lists">
				<div className="list-header">
					<span>{this.props.listsArray.header}</span>
				</div>
				<div className="list-cards">
					{ListElements}
				</div>
			</div>
		)
	}
}


class TrelloComponent extends Component {
	constructor() {
		super();

    this.handleDragOver = this.handleDragOver.bind(this)
		this.handleColumnDragStart = this.handleColumnDragStart.bind(this)
    this.handleOnDropColumn = this.handleOnDropColumn.bind(this)
    this.handleRowDragStart = this.handleRowDragStart.bind(this)
    this.handleOnDropRow = this.handleOnDropRow.bind(this)
   
		this.state = {
			listsArray: [
				{
					id: 1,
					header: "Lorem Header",
					listElements:[ 
						{
							id: 1,
							content: "Lorem ipsum dolor sit."
						},
						{
							id: 2,
							content: "Lorem ipsum dolor sit amet."
						}
					]
				},
				{
					id: 2,
					header: "Lorem Header",
					listElements: [ 
						{
							id: 1,
							content: "Lorem ipsum dolor sit amet, consectetur."
						},
						{
							id: 2,
							content: "Lorem ipsum dolor sit."
						}
					]
				},
				{
					id: 3,
					header: "Lorem Header",
					listElements: [
						{
							id: 1,
							content: "Lorem ipsum dolor sit amet. Hej hej"
						},
						{
							id: 2,
							content: "Lorem ipsum dolor sit amet."
						}
					]
				}
			],
			draggedRowId: null,
			draggedColumnId: null,
			draggedColumnRowId: null
		}
	}

  //-------------DRAG AND DROP FUNCTIONS COLUMN----------------------------
  handleDragOver (e){
    e.preventDefault();
    e.dataTransfer.dropEffect = 'move';
    console.log('Działam Drag Over')
  }
	handleColumnDragStart (e) {
    const id = e.target.id
		this.setState({ draggedColumnId: id})
		console.log('Działam Drag Start COLUMN')
		console.log(id)
		return id
  }
  handleOnDropColumn (e){
    const droppedColumnId = e.currentTarget.id
    console.log('Działam Drop COLUMN')
    console.log(droppedColumnId)
    console.log(this.state.draggedColumnId)
		const item = this.state.draggedColumnId.substring(2, this.state.draggedColumnId.length)
		console.log(item)
		if(item==='col'){
	    let reorderVal = { 
	      start: parseInt(this.state.draggedColumnId),
	      end: parseInt(droppedColumnId)
	    }
	    console.log(reorderVal.start)

	   const reorderIsCorrect = !isNaN(reorderVal.start) && !isNaN(reorderVal.end) && reorderVal.start !== reorderVal.end

	    if(reorderIsCorrect) {
	      const reorderItem = this.state.listsArray[reorderVal.start]
			    let newTrelloItems = []
			    this.state.listsArray.map((item, i) => {
						if( i === reorderVal.start){
							return
						}
			      // we need that if statement because
			      // the behaviour is determined if someone is dragging
			      // an item from higher to lower place on the list or vice versa
			      if(reorderVal.end < reorderVal.start) {
			        if(i === reorderVal.end) {
			          newTrelloItems.push(reorderItem)
			        }
			        newTrelloItems.push(item)
			      } else {
			        newTrelloItems.push(item)
			        if(i === reorderVal.end) {
			          newTrelloItems.push(reorderItem)
			        }
			      }
			    })
			    console.log(newTrelloItems)

			    this.setState({ listsArray: newTrelloItems })
	    }			
		}
    this.setState({ draggedColumnId: null })
  }
//-------------DRAG AND DROP FUNCTIONS ROWS----------------------------
	handleRowDragStart = (id) => (e) => {
    const idRow = e.target.id
		this.setState({ draggedRowId: idRow, draggedColumnRowId: id})
		console.log('Działam Drag Start ROW')
		console.log(idRow)
		console.log(this.state.draggedColumnRowId)
  }
  handleOnDropRow = (id) => (e) => {
    const droppedRowId = e.currentTarget.id
    console.log('Działam Drop ROW')
    console.log(droppedRowId)
    const columnId = id
    const dragColumnId = this.state.draggedColumnRowId
   	const parseOldColId = parseInt(dragColumnId)
    const parseId = parseInt(id)
    console.log(columnId)
    console.log(this.state.draggedColumnRowId)
    const item = this.state.draggedColumnId.substring(2, this.state.draggedColumnId.length)
		console.log(item)

		if(item==='row' && columnId===dragColumnId){
	    let reorderVal = { 
	      start: parseInt(this.state.draggedRowId),
	      end: parseInt(droppedRowId)
	    }
	    console.log(reorderVal.start)

	   const reorderIsCorrect = !isNaN(reorderVal.start) && !isNaN(reorderVal.end) && reorderVal.start !== reorderVal.end

	    if(reorderIsCorrect) {
	      const reorderItem = this.state.listsArray[parseOldColId].listElements[reorderVal.start]
			    let newTrelloItems = []
			    this.state.listsArray[parseOldColId].listElements.map((item, i) => {
						if( i === reorderVal.start){
							return
						}
			      // we need that if statement because
			      // the behaviour is determined if someone is dragging
			      // an item from higher to lower place on the list or vice versa
			      if(reorderVal.end < reorderVal.start) {
			        if(i === reorderVal.end) {
			          newTrelloItems.push(reorderItem)
			        }
			        newTrelloItems.push(item)
			      } else {
			        newTrelloItems.push(item)
			        if(i === reorderVal.end) {
			          newTrelloItems.push(reorderItem)
			        }
			      }
			    })
			    console.log(newTrelloItems)
		    
		  const listsArrayCopy = this.state.listsArray.slice()
	    
	   	listsArrayCopy[parseOldColId].listElements = newTrelloItems
	    
	    this.setState({ listsArray: listsArrayCopy })
	    }
		} else if (item==='row' && columnId!==dragColumnId){

	    let reorderVal = { 
	      start: parseInt(this.state.draggedRowId),
	      end: parseInt(droppedRowId)
	    }
	    console.log(reorderVal.start)

	   const reorderIsCorrect = !isNaN(reorderVal.start) && !isNaN(reorderVal.end)

	    if(reorderIsCorrect) {
	      const reorderItem = this.state.listsArray[parseOldColId].listElements[reorderVal.start]
			    let newTrelloItems = []
			    this.state.listsArray[parseOldColId].listElements.map((item, i) => {
						if( i === reorderVal.start){
							this.state.listsArray[parseOldColId].listElements.splice(reorderVal.start, 1)
						}
			    })
			    this.state.listsArray[parseId].listElements.map((item, i) => {
						if( i === reorderVal.end){
							this.state.listsArray[parseId].listElements.push(reorderItem)
						}
			    })
			  }
			}

    this.setState({ draggedRowId: null })
  }
  //------------------------------------RENDER--------------------------------
	render() {
		console.log(this.state.listsArray)
		return (
			<div className="board">
					{this.state.listsArray.map((items, i) => {
							return <List
												id={i}
												key={i}
												listsArray={items}
												handleOnDragOver={this.handleDragOver}
												handleOnDragColumnStart={this.handleColumnDragStart}
												handleOnDropColumn={this.handleOnDropColumn}
												handleOnDragRowStart={this.handleRowDragStart}
												handleOnDropRow={this.handleOnDropRow} />	
					})}
		</div>
		);
	}
}

export default TrelloComponent;