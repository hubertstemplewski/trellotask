import React, { Component } from 'react';
import logo from './logo.svg';
import TrelloComponent from './TrelloComponent';

class App extends Component {
  render() {
    return (
      <div className="App">
        <TrelloComponent />
      </div>
    );
  }
}

export default App;
